#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Aug 22 16:37 2023
@author: Nicolas Peteilh
"""


#!/usr/bin/env python3
"""
Created on Thu Jan 20 20:20:20 2020

@author: Conceptual Airplane Design & Operations (CADO team)
         Nicolas PETEILH, Pascal ROCHES, Nicolas MONROLIN, Thierry DRUOT
         Aircraft & Systems, Air Transport Department, ENAC
"""


import numpy as np
from marilib.utils import unit, earth
from marilib.aircraft.aircraft_root import Arrangement, Aircraft
from marilib.aircraft.requirement import Requirement
from marilib.aircraft.design import process
import matplotlib.pyplot as plt



# Configure airplane arrangement
# ---------------------------------------------------------------------------------------------------------------------
agmt = Arrangement(body_type = "fuselage",           # "fuselage"
                   wing_type = "classic",            # "classic"
                   wing_attachment = "low",          # "low" or "high"
                   stab_architecture = "classic",    # "classic", "t_tail" or "h_tail"
                   tank_architecture = "wing_box",   # "wing_box", "under_floor", "rear", "piggy_back" or "pods"
                   gear_architecture = "retractable",# "retractable", "bare_fixed"
                   number_of_engine = "twin",        # "twin" or "quadri"
                   nacelle_attachment = "wing",      # "wing", "pod" "rear" or "body_cones"
                   power_architecture = "tf",        # "tf", "tp", "ef", "ep", "pte", "pte_pod", "pte_piggy"
                   power_source = "fuel",            # "fuel", "fuel_cell", "fuel_cell_PEM", "battery"
                   fuel_type = "kerosene")           # "kerosene", "liquid_h2", "Compressed_h2" or "battery"

cruise_altp = unit.m_ft(35000.)
cruise_mach = 0.78
reqs = Requirement(n_pax_ref = 150.,
                   design_range = unit.m_NM(3000.),
                   cruise_mach = cruise_mach,
                   cruise_altp = cruise_altp)

ac = Aircraft("This_plane")     # Instantiate an Aircraft object

ac.factory(agmt, reqs)          # Configure the object according to Arrangement, WARNING : arrangement must not be changed after this line

# overwrite eventually default values for operational requirements
#-----------------------------------------------------------------------------------------------------------------------
# Take off
ac.requirement.take_off.tofl_req = 2300.

# Approach
ac.requirement.approach.app_speed_req = unit.convert_from("kt",137.)
# Climb
ac.requirement.mcl_ceiling.altp = cruise_altp
ac.requirement.mcl_ceiling.mach = cruise_mach
ac.requirement.mcl_ceiling.vz_req = unit.convert_from("ft/min",300.)

ac.requirement.mcr_ceiling.altp = cruise_altp
ac.requirement.mcr_ceiling.mach = cruise_mach
ac.requirement.mcr_ceiling.vz_req = unit.convert_from("ft/min",0.)

ac.requirement.oei_ceiling.altp = unit.m_ft(14000)

ac.requirement.time_to_climb.altp1 = unit.convert_from("ft",1500.)
ac.requirement.time_to_climb.cas1 = unit.convert_from("kt",180.)
ac.requirement.time_to_climb.altp2 = unit.convert_from("ft",10000.)
ac.requirement.time_to_climb.cas2 = unit.convert_from("kt",250.)
ac.requirement.time_to_climb.altp = cruise_altp
ac.requirement.time_to_climb.ttc_req = unit.convert_from("min",25.)


# overwrite default values for design parameters
ac.power_system.reference_thrust = unit.N_kN(120.26)

ac.airframe.wing.hld_type = 9.     # (max : 10)
ac.airframe.wing.aspect_ratio = 9.     # (max : 12)
ac.airframe.wing.area = 124.58


# Evaluation process
proc = "mda"

eval("process."+proc+"(ac)")  # Run MDA


# Evaluate aerodynamic polar

mach = cruise_mach
cz_list = np.arange(0.0, 0.75, 0.05)
pamb,tamb,tstd,dtodz = earth.atmosphere(cruise_altp, disa=0.0)
lod_list = [ac.aerodynamics.drag(pamb, tamb, mach, cz)[1] for cz in cz_list]

plt.plot(cz_list, lod_list)
plt.show()